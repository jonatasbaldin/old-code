from random import choice

class Doidera(object):
    def __init__(self):
        self.size = 30
        self.x0 = 0
        self.x1 = 0
        self.y0 = 0
        self.y1 = 0
        
        self.x = 0
        self.y = 0
        
        self.sw = 0
        self.red = 0
        self.green = 0
        self.blue = 0
        
        self.zen = [
            "beautiful",
            "ugly",
            "explicit",
            "implicit",
            "simple",
            "complex",
            "flat",
            "nested",
            "sparse",
            "dense",
            "readability",
            "practicality",
            "purity",
            "ambiguity",
            "now",
            "never",
        ]
        self.current_word = ""
        
        self.k = ""

    def update(self):
        for x in range(0, width, self.size):
            for y in range(0, height, self.size):
                self.x = x
                self.y = y
                
                self.red, self.green, self.blue = random(256), random(256), random(256)
                self.sw = random(20)
                
                if random(1) > 0.5:
                    self.x0, self.y0 = 0, 0
                    self.x1, self.y1 = self.size, self.size
                else:
                    self.x0, self.y0 = self.size, 0
                    self.x1, self.y1 = 0, self.size
                    
                self.draw()
                
        if keyPressed:
            textSize(random(100, 200))
            text(self.current_word, width / 4, random(height))
            
    def draw(self):
        stroke(self.red, self.green, self.blue)
        strokeWeight(self.sw)
        line(self.x0 + self.x, self.y0 + self.y, self.x1 + self.x, self.y1 + self.y)
    
    def pick_word(self):
        return choice(self.zen)

def setup():
    global doidera
    doidera = Doidera()
    current_word = doidera.pick_word()
    
    size(800, 800)
    # fullScreen()
    frameRate(15)

def draw():
    background(random(256), random(256), random(256))
    
    doidera.update()
    doidera.draw()
    
def keyPressed():
    if doidera.k != key:
        doidera.k = key
        doidera.current_word = doidera.pick_word()
