# Simple Golang Postgres
A _very_ simple Golang to connect to a PostgreSQL database.

## Usage
The application expects some environment variables:
- `host`: the PostgreSQL hostname
- `password`: the PostgreSQL password
- `port`: the PostgreSQL port
- `user`: the PostgreSQL user
- `db`: the PostgresqL database

## Running
```bash
docker run --rm -ti \ 
  -e host=<host> \ 
  -e password=<pass> \ 
  -e port=<port> \ 
  -e user=<user> \ 
  jonatasbaldin/simple-golang-postgres 
```

## License
[MIT](LICENSE).