package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/jackc/pgx/v4"
)

func main() {
	host := os.Getenv("host")
	if host == "" {
		log.Panic("unable to get the 'host' environment variable")
	}

	pass := os.Getenv("password")
	if pass == "" {
		log.Panic("unable to get the 'password' environment variable")
	}

	port := os.Getenv("port")
	if port == "" {
		log.Panic("unable to get the 'port' environment variable")
	}

	user := os.Getenv("user")
	if user == "" {
		log.Panic("unable to get the 'user' environment variable")
	}

	db := os.Getenv("db")
	if db == "" {
		db = "defaultdb"
	}

	dsn := fmt.Sprintf("user=%s password=%s host=%s port=%s database=%s", user, pass, host, port, db)

	config, err := pgx.ParseConfig(dsn)
	if err != nil {
		log.Panicf("unable to parse dsn config: %v", err)
	}

	conn, err := pgx.Connect(context.Background(), config.ConnString())
	if err != nil {
		log.Panicf("unable to connect to database: %v", err)
	}
	defer conn.Close(context.Background())

	var greeting string
	err = conn.QueryRow(context.Background(), "select 'Hello, world!'").Scan(&greeting)
	if err != nil {
		log.Panicf("queryRow failed: %v", err)
	}

	log.Println(greeting)

}
