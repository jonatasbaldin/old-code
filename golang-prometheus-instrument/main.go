// Nice articles on instrumenting Golang with Prometheus

// https://alex.dzyoba.com/blog/go-prometheus-service/
// https://loadium.com/blog/percentile-best-measure-for-response-time/
// https://tomgregory.com/the-four-types-of-prometheus-metrics/
// https://povilasv.me/prometheus-tracking-request-duration/#

package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func increasesEverySecond() {
	for {
		secondsFromStart.Inc()
		time.Sleep(1 * time.Second)
	}
}

func metricsHandler() gin.HandlerFunc {
	h := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func simpleCounterHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		add := c.Query("add")
		value, _ := strconv.ParseFloat(add, 64)
		simpleCounterTotal.Add(value)
	}
}

func simpleGaugeHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		_, incOk := c.GetQuery("inc")
		if incOk {
			simpleGaugeTotal.Inc()
		}

		_, descOk := c.GetQuery("dec")
		if descOk {
			simpleGaugeTotal.Dec()
		}

		add := c.Query("add")
		if add != "" {
			value, _ := strconv.ParseFloat(add, 64)
			simpleGaugeTotal.Add(value)
		}

		sub := c.Query("sub")
		if sub != "" {
			value, _ := strconv.ParseFloat(sub, 64)
			simpleGaugeTotal.Sub(value)
		}
	}
}

func pizzaOrderHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		flavor := c.Query("flavor")
		if flavor != "" {
			pizzaOrderedTotal.With(prometheus.Labels{"flavor": flavor}).Inc()
		}
	}
}

func waitHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		rand.Seed(time.Now().UnixNano())

		t := rand.Float64() * (5.1 - 0.1)
		waitRequestDuration.Observe(t)
		time.Sleep(time.Duration(t) * time.Second)

		c.String(200, fmt.Sprintf("slept for %fs\n", t))
	}

}

func countNumberTmpFiles() float64 {
	files, _ := ioutil.ReadDir("/tmp")
	return float64(len(files))
}

var (
	registry = prometheus.NewRegistry()
	factory  = promauto.With(registry)

	secondsFromStart = factory.NewCounter(prometheus.CounterOpts{
		Namespace: "myapp",
		Name:      "seconds_from_start_total",
		Help:      "The number of seconds elapsed since the start of the deamon",
	})

	simpleCounterTotal = factory.NewCounter(prometheus.CounterOpts{
		Namespace: "myapp",
		Name:      "simple_counter_total",
		Help:      "Hit /simple_counter?add=<num> to increase the counter",
	})

	simpleGaugeTotal = factory.NewGauge(prometheus.GaugeOpts{
		Namespace: "myapp",
		Name:      "simple_gauge_total",
		Help:      "Hit /simple_gauge?[inc|dec|add=num|sub=num] to change the gauge",
	})

	pizzaOrderedTotal = factory.NewCounterVec(prometheus.CounterOpts{
		Namespace: "myapp",
		Subsystem: "pizza",
		Name:      "ordered_total",
		Help:      "Hit /pizza/order?flavor=<your favorite flavor> to change the counter",
	}, []string{"flavor"})

	_ = factory.NewGaugeFunc(prometheus.GaugeOpts{
		Namespace: "myapp",
		Name:      "tmp_files_total",
		Help:      "Counts the number of files under /tmp",
	}, countNumberTmpFiles)

	waitRequestDuration = factory.NewHistogram(prometheus.HistogramOpts{
		Namespace: "myapp",
		Subsystem: "http",
		Name:      "wait_request_duration_seconds",
		Help:      "Receives the HTTP request duration from /wait",
		Buckets:   []float64{0.1, 0.5, 1, 2, 5},
	})
)

func main() {
	go increasesEverySecond()

	r := gin.Default()

	r.GET("/metrics", metricsHandler())
	r.GET("/simple_counter", simpleCounterHandler())
	r.GET("/simple_gauge", simpleGaugeHandler())

	r.GET("/pizza/order", pizzaOrderHandler())

	r.GET("/wait", waitHandler())

	r.Run(":8081")
}
