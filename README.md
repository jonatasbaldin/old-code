# old-code
All the repositories I had on GitHub, until June 6th 2022.

Most of the code here was not updated in a long time, so I'm just dumping all the repositories into the same folder. If, anyone of them becomes relevant again, I'll extract it somewhere else.

The `.git` folders inside were renamed to `.git-archive`, otherwise Gitea would identify them as git submodules.

## License
Each repo has its own license, please check it.
