module github.com/jonatasbaldin/brunchies

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/antchfx/htmlquery v1.0.0 // indirect
	github.com/antchfx/xmlquery v1.0.0 // indirect
	github.com/antchfx/xpath v1.0.0 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0 // indirect
	github.com/jonatasbaldin/ignore v0.0.0-20181031131724-8096127f6f65 // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/temoto/robotstxt v1.1.1 // indirect
	github.com/zmb3/spotify v0.0.0-20190725171427-5159bf56b13d
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
)
