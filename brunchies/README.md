# Brunchies
A web app to show the release history of the [Brunchies](https://open.spotify.com/playlist/4OyKDT6cLw96G7bd8nTfxD?si=VWAWuUYGSyW1wAe72JAOuw) playlist.

## Environment Vars
Setup the following environment variables:
- SPOTIFY_ID
- SPOTIFY_SECRET

## License
[MIT](./LICENSE)